@echo off

call %CI_PATH_SUFFIX%init_h.bat

echo %DELIM_STRING%
echo INFO: Loading config extensions

REM %APP_EXEC_PATH% Enterprise /F %BPATH%  /N %USER_NAME% /P %USER_PASS% /Execute %UNITOOLS_PATH%proc\адмЗагрузитьРасширенияВБазу.epf /C"%BUILD_PATH_INVERT%cfe/;%CI_PROJECT_DIR%connect_cfe_result.txt;ЗавершитьРаботуСистемы;" /AllowExecuteScheduledJobs –Off

set work_dir=%BUILD_PATH%%DELIVERY_PATH_CFE%
set connect_log_file=%CI_PROJECT_DIR%connect_cfe_result.txt

for %%C in (%work_dir%*.cfe) do call :cycle %%C

call %CI_PATH_SUFFIX%check_result.bat 1004

exit /b %ERRORLEVEL%

:cycle
    echo try to load extension "%~nx1"
    @echo on
    %APP_EXEC_PATH% Enterprise /F %BPATH%  /N %USER_NAME% /P %USER_PASS% /Execute %UNITOOLS_PATH%proc\ЗагрузитьРасширение.epf /C"%BUILD_PATH_INVERT%cfe/%~nx1;%connect_log_file%;ЗавершитьРаботуСистемы;" /AllowExecuteScheduledJobs –Off
    @echo off
    powershell /nologo /noprofile /command "get-content -encoding string '%connect_log_file%'|out-file -encoding utf8 'file_out.txt'"
    del /Q %connect_log_file%
    type file_out.txt
exit /b