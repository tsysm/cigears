@echo off

call %CI_PATH_SUFFIX%init_h.bat

echo %DELIM_STRING%
echo INFO: Run update routine in Enterpise mode 
time /t

IF %FLAG_BUILD_CF%==0 goto skipcode

@echo on
%APP_EXEC_PATH% Enterprise /F %BPATH%  /N %USER_NAME% /P %USER_PASS% /c ЗапуститьОбновлениеИнформационнойБазы;ЗавершитьРаботуСистемы /Execute %UNITOOLS_PATH%proc\ЗакрытьПредприятие.epf
@echo off

call %CI_PATH_SUFFIX%check_result.bat 1012

time /t
exit /b %ERRORLEVEL%

:skipcode
    echo Skip because preferences or nothing's changed
    exit /b
