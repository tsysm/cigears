call %CI_PATH_SUFFIX%init_h.bat

IF NOT EXIST %CI_PROJECT_DIR%public MKDIR %CI_PROJECT_DIR%public
echo report accessible on %CI_PAGES_URL%
REM C:\ProgramData\scoop\apps\allure\current\bin\allure.bat generate %CI_PROJECT_DIR%allurereport\%BUILD_NAME% -o %CI_PROJECT_DIR%public

echo copying allure's history from storage to build stage
IF NOT EXIST %CI_PROJECT_DIR%allurereport\history MKDIR %CI_PROJECT_DIR%allurereport\history
copy /Y %ALLURE_STORAGE_PATH%*.* %CI_PROJECT_DIR%allurereport\history\*.*

call %CI_PATH_SUFFIX%allure.bat

echo %DELIM_STRING%
echo copying allure's history from build stage to storage
IF NOT EXIST %ALLURE_STORAGE_PATH% MKDIR %ALLURE_STORAGE_PATH%
copy /Y %CI_PROJECT_DIR%public\history\*.* %ALLURE_STORAGE_PATH%*.*

call %CI_PATH_SUFFIX%check_result.bat 1070
exit /b %ERRORLEVEL%