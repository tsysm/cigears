@echo off

REM Позволяет подгружать частично файлы в конфигурацию из папки patch. В типовом процессе не используется, но в основном сценарии yml можно вызывать в любом 
REM месте. Например, сборка идет без патча, а перед тестированием снимается режим совместимости при помощи патча. Либо каталог patch может быть подмодулем

call %CI_PATH_SUFFIX%init_h.bat

echo %DELIM_STRING%
echo INFO: Applying config patches for cf
time /t

REM REM В тексте коммита можем передать флаг принудительного ребилда. Если указан, то конфигурация и расширения принудительно перезагрузятся, даже если ничего не поменялось.
REM FINDSTR /C:"[ci_build -f]" %CI_COMMIT_MESSAGE%
REM if NOT %errorlevel%==0 (
REM     echo Found [ci_build -f] in commit message. Force build is enabled!
REM     set errorlevel=0
REM     set FLAG_BUILD_CF=1
REM )

IF %FLAG_BUILD_CF%==0 goto skipcode

REM REM В тексте коммита можем передать флаг оптимизации ребилда. Если указан, то проверяем, были ли изменения в каталоге конфигурации в текущем коммите по сравнению с предыдущим. 
REM REM Если не было, то и загружать конфигурацию не будем.
REM FINDSTR /C:"[ci_build -o]" %CI_COMMIT_MESSAGE%
REM if NOT %errorlevel%==0 (
REM     echo Found [ci_build -o] in commit message. Optimized build is enabled!
REM     set errorlevel=0

REM echo last commit sha is %LAST_COMMIT_SHA%
REM if not defined LAST_COMMIT_SHA (set COMMIT_TEXT=%CI_COMMIT_SHA%) else (set COMMIT_TEXT=%LAST_COMMIT_SHA%..%CI_COMMIT_SHA%)

REM @echo on
REM > diff.txt git log %COMMIT_TEXT% --name-only --pretty=format:'' -- cf
REM @echo off

REM for %%I in (diff.txt) do (
REM     echo size of diffs is %%~zI
REM     if %%~zI==0 (
REM         echo Nothing changed from last commit %LAST_COMMIT_SHA%
REM         set errorlevel=0
REM         set FLAG_BUILD_CF=0
REM         goto skipcode
REM     )
REM )

if %CI_APP_VERSION_DIGITS% GEQ 8370000 (
    @echo on
    %APP_EXEC_PATH% designer /F "%bpath%"  /N %USER_NAME% /P %USER_PASS% /ManageCfgSupport -disableSupport -force
    @echo off
) else (echo parameter /ManageCfgSupport available in version higher 8.3.7.xxxx current version is %CI_APP_VERSION%)

@echo on
%APP_EXEC_PATH% designer /F "%bpath%"  /N %USER_NAME% /P %USER_PASS% /LoadConfigFromFiles %CI_PROJECT_DIR%%DELIVERY_PATH_CF% -listFile %CI_PROJECT_DIR%patch\patch.txt /UpdateDBCfg %CONNECT_POSTFIX_STRING%
@echo off

call %CI_PATH_SUFFIX%check_result.bat 1002

time /t
exit /b %ERRORLEVEL%

:skipcode
    echo Skip loading because preferences or nothing's changed
    exit /b
