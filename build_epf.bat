@echo off

call %CI_PATH_SUFFIX%init_h.bat

echo %DELIM_STRING%
echo INFO: connect external processors into base
time /t
IF %FLAG_BUILD_EPF%==0 goto skipcode

set work_dir=%CI_PROJECT_DIR%%DELIVERY_PATH_PROC%
for /d %%B in (%work_dir%*) do ( 
    echo dir %%B
    for %%C in ("%%B\*.xml") do (
        echo %%~nxC
        @echo on
        %APP_EXEC_PATH% DESIGNER /F %BPATH% /N %USER_NAME% /P %USER_PASS% /LoadExternalDataProcessorOrReportFromFiles %%C %BUILD_PATH%%DELIVERY_PATH_PROC%%%~nC 
        @echo off
        call %CI_PATH_SUFFIX%check_result.bat 1004
    )
)

REM %APP_EXEC_PATH% Enterprise /F %BPATH%  /N %USER_NAME% /P %USER_PASS% /Execute %UNITOOLS_PATH%proc\адмПодключениеДополнительныхОбработок.epf /C"%BUILD_PATH%%DELIVERY_PATH_PROC%;%CI_PROJECT_DIR%conExtProc_result.txt;ЗавершитьРаботуСистемы;" /AllowExecuteScheduledJobs –Off /TESTMANAGER
call %CI_PATH_SUFFIX%check_result.bat 1004

exit /b %ERRORLEVEL%

:skipcode
    echo Skip loading because preferences
    exit /b
