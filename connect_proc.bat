@echo off

call %CI_PATH_SUFFIX%init_h.bat

echo %DELIM_STRING%
echo INFO: connect external processors into base
time /t

@echo on
%APP_EXEC_PATH% Enterprise /F%BPATH% /N%USER_NAME% /P%USER_PASS% /Execute %UNITOOLS_PATH%proc\адмПодключениеДополнительныхОбработок.epf /C"%BUILD_PATH%proc;%CI_PROJECT_DIR%conExtProc.txt;ЗавершитьРаботуСистемы;" /AllowExecuteScheduledJobs –Off
@echo off

call %CI_PATH_SUFFIX%check_result.bat 1004

exit /b %ERRORLEVEL%