@echo off

call %CI_PATH_SUFFIX%init_h.bat

echo %DELIM_STRING%
echo INFO: Loading config extensions from src
IF NOT EXIST %BUILD_PATH%%DELIVERY_PATH_CFE% MKDIR %BUILD_PATH%%DELIVERY_PATH_CFE%

REM REM В тексте коммита можем передать флаг принудительного ребилда. Если указан, то конфигурация и расширения принудительно перезагрузятся, даже если ничего не поменялось.
REM FINDSTR /C:"[ci_build -f]" %CI_COMMIT_MESSAGE%
REM if NOT %errorlevel%==0 (
REM     echo Found [ci_build -f] in commit message. Force build is enabled!
REM     set errorlevel=0
REM     set FLAG_BUILD_CFE=1
REM )

IF %FLAG_BUILD_CFE%==0 goto skipcode

REM REM В тексте коммита можем передать флаг оптимизации ребилда. Если указан, то проверяем, были ли изменения в каталоге конфигурации в текущем коммите по сравнению с предыдущим. 
REM REM Если не было, то и загружать конфигурацию не будем.
REM FINDSTR /C:"[ci_build -o]" %CI_COMMIT_MESSAGE%
REM if NOT %errorlevel%==0 (
REM     echo Found [ci_build -o] in commit message. Optimized build is enabled!
REM     set errorlevel=0
echo last commit sha is %LAST_COMMIT_SHA%
if not defined LAST_COMMIT_SHA (set COMMIT_TEXT=%CI_COMMIT_SHA%) else (set COMMIT_TEXT=%LAST_COMMIT_SHA%..%CI_COMMIT_SHA%)

set work_dir=%CI_PROJECT_DIR%%DELIVERY_PATH_CFE%
for /d %%B in (%work_dir%*) do ( 
    echo dir %%B
    @echo on
    > diff.txt git log %COMMIT_TEXT% --name-only --pretty=format:'' -- cfe/%%~nB
    @echo off
    for %%I in (diff.txt) do (
        echo size of diffs is %%~zI
        if %%~zI NEQ 0 (
            @echo on
            %APP_EXEC_PATH% designer /F "%bpath%" /N %USER_NAME% /P %USER_PASS% /LoadConfigFromFiles %CI_PROJECT_DIR%%DELIVERY_PATH_CFE%%%~nB -Extension %%~nB %CONNECT_POSTFIX_STRING%
            call %CI_PATH_SUFFIX%dump_cfe_file.bat %%~nB
            @echo off
        ) else (echo extensions %%~nB wasnt changed)
    )
)

call %CI_PATH_SUFFIX%check_result.bat 1020

time /t
exit /b %ERRORLEVEL%

:skipcode
    REM echo Skip loading because preferences or nothing's changed
    exit /b
