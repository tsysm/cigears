powershell /nologo /noprofile /command "get-content -encoding string '%logfilename%'|out-file -encoding utf8 'file_out.txt'"
del /Q %logfilename%
REN file_out.txt lasttest.txt
set /p return_value=< %retfilename%
echo return value = %return_value%
IF %return_value% NEQ 0 type %logfilename%
IF %return_value% NEQ 0 exit %1
IF %ERRORLEVEL% NEQ 0 exit %ERRORLEVEL%
exit /b