call %CI_PATH_SUFFIX%init_h.bat
CHOICE /T 60 /C ync /CS /D y /M "load initial DT?"
set result=%errorlevel%
set errorlevel=0
echo %errorlevel%
if %result% EQU 1 call init_image.bat

CHOICE /T 60 /C ync /CS /D y /M "load config files into base with updating base?"
set result=%errorlevel%
set errorlevel=0
echo %errorlevel%
if %result% EQU 1 call compile.bat
if %result% EQU 1 call DBUpdate.bat

CHOICE /T 60 /C ync /CS /D y /M "load config extensions into base?"
set result=%errorlevel%
set errorlevel=0
echo %errorlevel%
if %result% EQU 1 call connect_conf_ext.bat

CHOICE /T 60 /C ync /CS /D y /M "load external processors into base?"
set result=%errorlevel%
set errorlevel=0
echo %errorlevel%
if %result% EQU 1 call connect_ext_proc.bat

CHOICE /T 60 /C ync /CS /D y /M "run test routine?"
set result=%errorlevel%
set errorlevel=0
echo %errorlevel%
if %result% EQU 1 call run_vanessa.bat

CHOICE /T 60 /C ync /CS /D y /M "close?"