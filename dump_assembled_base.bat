@echo off

call %CI_PATH_SUFFIX%init_h.bat

echo %DELIM_STRING%
echo INFO: dump assembled base into dt
time /t

set ASSEMBLED_BASE_IMAGE_NAME=assembled_image.dt

IF NOT EXIST %CI_PROJECT_DIR%%DELIVERY_PATH_UNLOAD% MKDIR %CI_PROJECT_DIR%%DELIVERY_PATH_UNLOAD%
@echo on
%APP_EXEC_PATH% DESIGNER /F%bpath% /N%USER_NAME% /P%USER_PASS% /DumpIB %CI_PROJECT_DIR%%DELIVERY_PATH_UNLOAD%%ASSEMBLED_BASE_IMAGE_NAME% %CONNECT_POSTFIX_STRING%
@echo off
call %CI_PATH_SUFFIX%check_result.bat 1010

time /t
exit /b %ERRORLEVEL%
