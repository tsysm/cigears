@echo off
REM chcp 1251
echo try to unload cfe with name %1

@echo on
%APP_EXEC_PATH% DESIGNER /F%bpath% /N%USER_NAME% /P%USER_PASS% /DumpCfg %BUILD_PATH%%DELIVERY_PATH_CFE%%1.cfe -Extension %1 %CONNECT_POSTFIX_STRING%
@echo off

call %CI_PATH_SUFFIX%check_result.bat 1020

echo ..SUCCESS

time /t
exit /b %ERRORLEVEL%
