REM @echo off

call %CI_PATH_SUFFIX%init_h.bat

REM По умолчанию полагаем, что работаем через add так исторически сложилось, возможно дальше будем работать с va и в какой-то момент она станет дефолтной
set BDD_EXEC_NAME=bddRunner.epf
set BDD_PATH=%UNITOOLS_PATH%test\add\

REM Если указано в настройках, что работаем через VA, значит переопределяем настройки на VA
if %BDD_EXEC%==va (
    set BDD_EXEC_NAME=vanessa-automation.epf
    set BDD_PATH=%UNITOOLS_PATH%test\va\
)
if %BDD_EXEC%==add (
    set BDD_EXEC_NAME=bddRunner.epf
    set BDD_PATH=%UNITOOLS_PATH%test\add\
)

set BDD_PATH_DBL=%BDD_PATH:\=\\%
set BDD_PATH_INVERT=%BDD_PATH:\=/%
set BDD_EXEC_PATH=%BDD_PATH%%BDD_EXEC_NAME%

echo { >add_prefs.json
if %BDD_EXEC%==add echo "$schema":"https://raw.githubusercontent.com/silverbulleters/vanessa-runner/develop/behavior-schema.json", >>add_prefs.json
echo "ИмяСборки":"%BUILD_NAME%", >>add_prefs.json
echo "ВерсияПлатформы":"%CI_APP_VERSION%", >>add_prefs.json
echo "ВыводитьСообщенияВФайл": "%CI_PROJECT_DIR_DBL%tests.txt", >>add_prefs.json
echo "КаталогПоискаВерсииПлатформы":"C:\\Progra~2\\1cv8", >>add_prefs.json
echo "СтрокаПодключенияКБазе":"%BPATH_DBL%", >>add_prefs.json
echo "EpfДляИнициализацияБазы": "%BDD_PATH_INVERT%tools/epf/init.epf", >>add_prefs.json
echo "ПараметрыДляИнициализацияБазы": "%BDD_PATH_INVERT%tools/epf/init.json", >>add_prefs.json
if %BDD_EXEC%==add echo "ПутьКVanessaBehavior": "%BDD_PATH_INVERT%%BDD_EXEC_NAME%", >>add_prefs.json
if %BDD_EXEC%==va echo "ПутьКVanessaAutomation": "%BDD_PATH_INVERT%%BDD_EXEC_NAME%", >>add_prefs.json
if %BDD_EXEC%==va echo "ТаймаутЗапуска1С": "60", >>add_prefs.json
echo "КаталогФич": "%CI_PROJECT_DIR_INVERT%features", >>add_prefs.json
echo "КаталогиБиблиотек":[ >>add_prefs.json
echo "%BDD_PATH_INVERT%features/libraries" >>add_prefs.json
echo ], >>add_prefs.json

echo "КлиентыТестирования": [ >>add_prefs.json
echo     { >>add_prefs.json
echo         "Имя": "Этот клиент", >>add_prefs.json
echo         "ПутьКИнфобазе": "File=%BPATH_INVERT%", >>add_prefs.json
echo         "ДопПараметры": "/N %USER_NAME% /P %USER_PASS% /AllowExecuteScheduledJobs –Off", >>add_prefs.json
echo         "ТипКлиента": "Тонкий", >>add_prefs.json
echo         "ИмяКомпьютера": "localhost", >>add_prefs.json
echo         "АктивизироватьСтроку": "Истина" >>add_prefs.json
echo     }, >>add_prefs.json
echo     { >>add_prefs.json
REM клиент ТестированиеПоведения используется в тестах, чтобы не переделывать оставляем его в настройках.
echo         "Имя": "ТестированиеПоведения", >>add_prefs.json
echo         "ПутьКИнфобазе": "File=%BPATH_INVERT%", >>add_prefs.json
echo         "ДопПараметры": "/N %USER_NAME% /P %USER_PASS% /AllowExecuteScheduledJobs –Off", >>add_prefs.json
echo         "ТипКлиента": "Тонкий", >>add_prefs.json
echo         "ИмяКомпьютера": "localhost", >>add_prefs.json
echo         "АктивизироватьСтроку": "Ложь" >>add_prefs.json
echo     }, >>add_prefs.json
echo     { >>add_prefs.json
echo         "Имя": "РуководительОК", >>add_prefs.json
echo         "ПутьКИнфобазе": "File=%BPATH_INVERT%", >>add_prefs.json
echo         "ДопПараметры": "/N %USER_NAME% /P %USER_PASS% /AllowExecuteScheduledJobs –Off", >>add_prefs.json
echo         "ТипКлиента": "Тонкий", >>add_prefs.json
echo         "ИмяКомпьютера": "localhost", >>add_prefs.json
echo         "АктивизироватьСтроку": "Ложь" >>add_prefs.json
echo     } >>add_prefs.json
echo ], >>add_prefs.json
echo "ВыполнитьСценарии": "Истина", >>add_prefs.json
echo "ЗавершитьРаботуСистемы": "Истина", >>add_prefs.json
echo "ЗакрытьTestClientПослеЗапускаСценариев": "Истина", >>add_prefs.json
echo "ДелатьЛогВыполненияСценариевВЖР": "Ложь", >>add_prefs.json
echo "ДелатьОтчетВФорматеАллюр": "Истина", >>add_prefs.json
echo "ДелатьОтчетВФорматеjUnit": "Ложь", >>add_prefs.json
echo "ДелатьОтчетВФорматеCucumberJson": "Ложь", >>add_prefs.json
echo "ДелатьЛогВыполненияСценариевВТекстовыйФайл": "Истина", >>add_prefs.json
echo "КаталогOutputAllureБазовый": "%CI_PROJECT_DIR_INVERT%allurereport", >>add_prefs.json
echo "КаталогOutputjUnit": "%CI_PROJECT_DIR_INVERT%junitreport", >>add_prefs.json
echo "КаталогOutputCucumberJson": "%CI_PROJECT_DIR_INVERT%cucumber", >>add_prefs.json
echo "СоздаватьПодкаталогВКаталогеAllureДляЭтойСборки": "Ложь", >>add_prefs.json
echo "КаталогOutputСкриншоты": "%CI_PROJECT_DIR_INVERT%ScreenShots", >>add_prefs.json
echo "КомандаСделатьСкриншот": "\"C:\\Program Files (x86)\\IrfanView\\i_view32.exe\" /capture=1 /convert=", >>add_prefs.json
echo "ДелатьСкриншотПриВозникновенииОшибки": "Ложь", >>add_prefs.json
echo "ВыгружатьСтатусВыполненияСценариевВФайл": "Истина", >>add_prefs.json
echo "ИмяФайлаЛогВыполненияСценариев": "%CI_PROJECT_DIR_INVERT%log.txt", >>add_prefs.json
echo "ДобавлятьКИмениСценарияУсловияВыгрузки": "Истина", >>add_prefs.json
echo "ПутьКФайлуДляВыгрузкиСтатусаВыполненияСценариев": "%CI_PROJECT_DIR_INVERT%return_value.txt", >>add_prefs.json
echo "ВерсияПлатформыДляГенерацииEPF": "%APP_WORK_PATH_INVERT%", >>add_prefs.json
echo "СписокТеговИсключение":[ >>add_prefs.json
echo "IgnoreOnCIMainBuild", >>add_prefs.json
echo "IgnoreOn836", >>add_prefs.json
echo "IgnoreOnUFBuilds", >>add_prefs.json
echo "Draft", >>add_prefs.json
echo "init", >>add_prefs.json
echo "Ignore" >>add_prefs.json
echo ], >>add_prefs.json
echo "ВыполнятьШагиАссинхронно":"Ложь" >>add_prefs.json
echo } >>add_prefs.json


echo %DELIM_STRING%
echo INFO: begin testing program's behavior
time /t

if %CI_APP_VERSION_DIGITS% LEQ 8380000 (
    echo test functional available on platform higher 8.3.8.xxxx current version is %CI_APP_VERSION%
    goto skipcode
)

echo run add test in enterprise mode
@echo on
%APP_EXEC_PATH% Enterprise /F %BPATH%  /N %USER_NAME% /P %USER_PASS% /Execute %BDD_EXEC_PATH% /C"StartFeaturePlayer;VBParams=%CI_PROJECT_DIR_INVERT%add_prefs.json;workspaceRoot=%CI_PROJECT_DIR_INVERT%" /AllowExecuteScheduledJobs –Off /TESTMANAGER
@echo off

echo report accessible on %CI_PAGES_URL%

call %CI_PATH_SUFFIX%check_result.bat 1040
exit /b %ERRORLEVEL%

:skipcode
    echo Skip testing because preferences
    exit /b