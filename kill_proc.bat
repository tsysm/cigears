@echo off
chcp 65001

call %CI_PATH_SUFFIX%init_h.bat

set HANDLE_PATH=%UNITOOLS_PATH%exec\handle\
if %APP_EXEC_BITS%==32 (
    set HANDLE_NAME=handle.exe) else (set HANDLE_NAME=handle64.exe)
@echo on
%HANDLE_PATH%%HANDLE_NAME% %BPATH% -nobanner > %CI_PROJECT_DIR%handles.txt
@echo off
set curHandle=0
FOR /F "tokens=1,3 " %%h in (%CI_PROJECT_DIR%handles.txt) do (
    echo detected handling process %%i %%h
    @echo on
    taskkill /PID %%i /F
    @echo off
    set ERRORLEVEL=999
)
IF %ERRORLEVEL% NEQ 0 exit %ERRORLEVEL%
exit /b