@echo off

call %CI_PATH_SUFFIX%init_h.bat

echo %DELIM_STRING%
echo INFO: dump cfe to files
time /t

REM Выгружаем расширения в файлы. Для этого сначала выгружаем список расширений из базы, затем по этому списку создаем имена файлов и выгружаем в них расширения

set CFE_LIST_FILENAME=%logfilename%

IF NOT EXIST %BUILD_PATH%%DELIVERY_PATH_CFE% MKDIR %BUILD_PATH%%DELIVERY_PATH_CFE%

@echo on
%APP_EXEC_PATH% DESIGNER /F%bpath% /N%USER_NAME% /P%USER_PASS% /DumpDBCfgList -AllExtensions %CONNECT_POSTFIX_STRING%
@echo off

call %CI_PATH_SUFFIX%check_result.bat 1020

for /F "eol=#" %%A in (%CFE_LIST_FILENAME%) do call %CI_PATH_SUFFIX%dump_cfe_file.bat %%A

REM Загрузим расширения из только что созданных бинарников через пользовательский режим. Безопасный режим будет отключен.
@echo on
%APP_EXEC_PATH% Enterprise /F %BPATH%  /N %USER_NAME% /P %USER_PASS% /Execute %UNITOOLS_PATH%proc\адмЗагрузитьРасширенияВБазу.epf /C"%BUILD_PATH_INVERT%cfe/;%CI_PROJECT_DIR%connect_conf_extentions_result.txt;ЗавершитьРаботуСистемы;" /AllowExecuteScheduledJobs –Off
@echo off

time /t
exit /b %ERRORLEVEL%
