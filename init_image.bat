@echo off

call %CI_PATH_SUFFIX%init_h.bat

echo %DELIM_STRING%
echo INFO: begin create temp base at path %bpath%
time /t

REM По умолчанию берем начальный образ из папки Primary
set IMAGE_PATH=%EMPTY_BASE_IMAGE_PATH% 
REM если в папке клиента уже есть папка с названием текущей ветки и там лежит выгрузка, то берем ее вместо начального образа
IF EXIST %DELIVERY_CLIENT_PATH%%CI_COMMIT_REF_NAME%\%DELIVERY_PATH_UNLOAD%%EMPTY_BASE_IMAGE_NAME% set IMAGE_PATH=%DELIVERY_CLIENT_PATH%%CI_COMMIT_REF_NAME%\%DELIVERY_PATH_UNLOAD%%EMPTY_BASE_IMAGE_NAME%
REM но если в репозитории раннера уже есть папка unloads и в ней лежит образ базы, то берем его
IF EXIST %CI_PROJECT_DIR%%DELIVERY_PATH_UNLOAD%%EMPTY_BASE_IMAGE_NAME% set IMAGE_PATH=%CI_PROJECT_DIR%%DELIVERY_PATH_UNLOAD%%EMPTY_BASE_IMAGE_NAME%

DEL /Q %bpath%*.*

@echo on
%APP_EXEC_PATH% CREATEINFOBASE File=%bpath%; /UseTemplate %IMAGE_PATH% %CONNECT_POSTFIX_STRING%
@echo off

call %CI_PATH_SUFFIX%check_result.bat 1001

time /t
exit /b %ERRORLEVEL%
