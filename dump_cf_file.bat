@echo off

call %CI_PATH_SUFFIX%init_h.bat

echo %DELIM_STRING%
echo INFO: dump cf to files
time /t

REM Выгружаем конфигурацию в файл cf.

IF NOT EXIST %BUILD_PATH%%DELIVERY_PATH_CF% MKDIR %BUILD_PATH%%DELIVERY_PATH_CF%

@echo on
%APP_EXEC_PATH% DESIGNER /F%bpath% /N%USER_NAME% /P%USER_PASS% /DumpDBCfg %BUILD_PATH%%DELIVERY_PATH_CF%1cv8.cf %CONNECT_POSTFIX_STRING%
@echo off

call %CI_PATH_SUFFIX%check_result.bat 1020

time /t
exit /b %ERRORLEVEL%
