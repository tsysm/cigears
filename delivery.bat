@echo off

call %CI_PATH_SUFFIX%init_h.bat

echo %DELIM_STRING%
echo INFO: delivering %1 into clients folder
time /t

REM Если предыдущая задача выпала с ошибкой, то будем скидывать все билды и архив в подпапку FAIL\номер_комита
set DELIVERY_SUFFIX=%CI_COMMIT_REF_NAME%\
if %CI_FAIL%==1 set DELIVERY_SUFFIX=FAIL\%CI_COMMIT_SHORT_SHA%\

if %1==base set SOURCE_PATH_FULL=%CI_PROJECT_DIR%%DELIVERY_PATH_UNLOAD%
if %1==cf set SOURCE_PATH_FULL=%BUILD_PATH%%DELIVERY_PATH_CF%
if %1==cfe set SOURCE_PATH_FULL=%BUILD_PATH%%DELIVERY_PATH_CFE%
if %1==proc set SOURCE_PATH_FULL=%BUILD_PATH%%DELIVERY_PATH_PROC%

if %1==base set DELIVERY_PATH_FULL=%DELIVERY_CLIENT_PATH%%DELIVERY_SUFFIX%%DELIVERY_PATH_UNLOAD%
if %1==cf set DELIVERY_PATH_FULL=%DELIVERY_CLIENT_PATH%%DELIVERY_SUFFIX%%DELIVERY_PATH_CF%
if %1==cfe set DELIVERY_PATH_FULL=%DELIVERY_CLIENT_PATH%%DELIVERY_SUFFIX%%DELIVERY_PATH_CFE%
if %1==proc set DELIVERY_PATH_FULL=%DELIVERY_CLIENT_PATH%%DELIVERY_SUFFIX%%DELIVERY_PATH_PROC%

echo path to delivery is %DELIVERY_PATH_FULL%

IF NOT EXIST %DELIVERY_PATH_FULL% MKDIR %DELIVERY_PATH_FULL%

@echo on
move /Y %SOURCE_PATH_FULL%*.* %DELIVERY_PATH_FULL%
@echo off

REM if %CI_FAIL%==1 set LAST_COMMIT_PATHNAME=%DELIVERY_PATH_FULL%

if %1==base (
    REM set /p strTemp=%CI_COMMIT_SHA%
    REM If "%CI_COMMIT_SHA:~-1%"==" " Set strTemp=%CI_COMMIT_SHA:~0,-1% 
    REM REM @echo on
    REM > %LAST_COMMIT_PATHNAME% <nul set /p strTemp=%CI_COMMIT_SHA%
    REM REM @echo off
    del /Q %DELIVERY_PATH_FULL%*.commit
    >%DELIVERY_PATH_FULL%%CI_COMMIT_SHA%.commit echo 1
)
call %CI_PATH_SUFFIX%check_result.bat 1010

time /t
exit /b %ERRORLEVEL%
