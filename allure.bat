call %CI_PATH_SUFFIX%init_h.bat

echo generate allure static site with test reports
@echo on
C:\ProgramData\scoop\apps\allure\current\bin\allure.bat generate %CI_PROJECT_DIR%allurereport -o %CI_PROJECT_DIR%public
@echo off

exit /b