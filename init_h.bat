chcp 65001
REM Инициализируем переменные, которые пригодятся нам в основных сценариях

IF defined ENV_INITIALIZED exit /b

echo init main variables

set DELIM_STRING===============================================================================

for /f "tokens=1-4 delims=." %%A in ("%CI_APP_VERSION%") do (set CI_APP_VERSION_DIGITS=%%A%%B%%C%%D)

IF %CI_RUNNER_DESCRIPTION%==adma.dev.grunner set DELIVERY_MAIN_PATH=F:\Dev1c\
IF %CI_RUNNER_DESCRIPTION%==adma.reg.grunner set DELIVERY_MAIN_PATH=C:\Dev1c\
IF %CI_RUNNER_DESCRIPTION%==vertex.prod set DELIVERY_MAIN_PATH=C:\Users\1CProg\Downloads\Dev1c\


IF NOT exist init_h_repo.bat (
    echo you dont have init_h_repo.bat in your repository
    exit 1999
)

call init_h_repo.bat

REM КОНЕЦ СЕКЦИИ

IF NOT defined CI_PROJECT_DIR set CI_PROJECT_DIR=%CD:\=/%
REM IF SET_TO_CRRNT_CAT EQU 1 set CI_PROJECT_DIR=%CD:\=/%

rem по умолчанию запускаем 32 битную платформу, если в .gitlab-ci.yml не указано обратное.
IF NOT defined APP_EXEC_BITS set APP_EXEC_BITS=32
set CI_PROJECT_DIR_INVERT=%CI_PROJECT_DIR%/
set CI_PROJECT_DIR_INVERT=%CI_PROJECT_DIR_INVERT:\=/%
set CI_PROJECT_DIR=%CI_PROJECT_DIR:/=\%\
set CI_PROJECT_DIR_DBL=%CI_PROJECT_DIR:\=\\%
set BPATH=%CI_PROJECT_DIR%BASE\
set TESTER_PATH=%CI_PROJECT_DIR%tester\
set BPATH_DBL=%CI_PROJECT_DIR_DBL%BASE\\
set BPATH_INVERT=%BPATH:\=/%
echo path to workfiles %BPATH%


echo delivery's main path is %DELIVERY_MAIN_PATH%

REM set BDD_PATH=%UNITOOLS_PATH%test\add\
REM set BDD_PATH_DBL=%BDD_PATH:\=\\%
REM set BDD_PATH_INVERT=%BDD_PATH:\=/%
REM set BDD_EXEC_PATH=%BDD_PATH%bddRunner.epf

set BDD_PATH=%UNITOOLS_PATH%test\va\
set BDD_PATH_DBL=%BDD_PATH:\=\\%
set BDD_PATH_INVERT=%BDD_PATH:\=/%
set BDD_EXEC_PATH=%BDD_PATH%vanessa-automation.epf

set BUILD_PATH=%CI_PROJECT_DIR%build\
set BUILD_PATH_DBL=%BUILD_PATH:\=\\%
set BUILD_PATH_INVERT=%BUILD_PATH:\=/%

set BUILD_NAME=%CI_COMMIT_REF_SLUG%

set LAST_COMMIT_PATHNAME=%DELIVERY_CLIENT_PATH%%CI_COMMIT_REF_NAME%\%DELIVERY_PATH_UNLOAD%*.commit
for %%C in (%LAST_COMMIT_PATHNAME%) do (
    echo last comit is %%~nC
    set LAST_COMMIT_SHA=%%~nC
)


IF NOT defined CI_APP_VERSION set CI_APP_VERSION=8.3.13.1513

set APP_WORK_PATH=C:\Progra~2\1cv8\%CI_APP_VERSION%\bin\
if %APP_EXEC_BITS% NEQ 32 set APP_WORK_PATH=C:\Progra~1\1cv8\%CI_APP_VERSION%\bin\

set APP_WORK_PATH_INVERT=%APP_WORK_PATH:\=/%

set APP_EXEC_PATH=%APP_WORK_PATH%1cv8.exe
set APP_EXEC_PATH_INVERT=%APP_EXEC_PATH:\=/%

echo execute path = %APP_EXEC_PATH%

set EMPTY_BASE_IMAGE_PATH=%DELIVERY_CLIENT_PATH%primary\%DELIVERY_PATH_UNLOAD%%EMPTY_BASE_IMAGE_NAME%
echo image path is %EMPTY_BASE_IMAGE_PATH%

REM set allure storage path
set ALLURE_STORAGE_PATH=%DELIVERY_CLIENT_PATH%primary\allure\history\

set logfilename=%CI_PROJECT_DIR%lasttest.txt
set logfilename_CLIENT=%CI_PROJECT_DIR:\=//%lasttest_client.txt
set retfilename=%CI_PROJECT_DIR%return_value.txt
REM set CONNECT_POSTFIX_STRING=/UsePrivilegedMode /DisableStartupDialogs /DisableStartupMessages /L en /Out %logfilename% /DumpResult %retfilename%
set CONNECT_POSTFIX_STRING=/UsePrivilegedMode /DisableStartupDialogs /DisableStartupMessages /L en /AllowExecuteScheduledJobs –Off /Out %logfilename% /DumpResult %retfilename%
set return_value=0

rem установим переменную по которой будем опознавать, делалась ли уже инициализация переменных. Если да, то повторно не делаем.
set ENV_INITIALIZED=1

cd /d %CI_PROJECT_DIR%

exit /b